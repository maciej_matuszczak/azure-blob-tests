import { Component } from "react";
import Router from "next/router";
import * as blob from "../azure-storage/azure-storage.blob.min";
import { SAS_TOKEN, SAS_URL } from "../consts";
import { ReactMic } from 'react-mic';

class LoginForm extends Component {
  state = {
    login: null,
    password: null,
    recording: false,
    file: null,
    error: null
  };

  sendFile = () => {
    const date = new Date().toISOString();
    const service = blob.createBlobServiceWithSas(SAS_URL, SAS_TOKEN);
    service.createContainerIfNotExists(
      "default",
      {
        publicAccessLevel: "blob"
      },
      (containerError, containerResult, containerResponse) => {
        if (!containerError) {
          if (!this.state.file) {
            this.setState({ error: "Wybierz plik" });
            return;
          }
          service.createBlockBlobFromBrowserFile(
            "default",
            date,
            this.convertToFile(this.state.file.blob, date),
            (error, result, response) => {
              if (!error) {
                // console.log("SUCCESS", result);
                this.clear();
                Router.push("/");
              } else {
                this.setState({ error });
              }
            }
          );
        } else {
          this.setState({ error: containerError });
        }
      }
    );
  };

  convertToFile = (blobObject, name) => {
    return new File([blobObject], name)
  }

  submitForm = event => {
    event.preventDefault();
    //  console.log("SUBMIT", this.state);
    this.sendFile();
  };

  handleChange = ({ target }) => {
    this.setState({ [target.name]: target.value });
  };

  onData = (recordedBlob) => {
    // console.log('Recorded file: ', recordedBlob);
  }

  onStop = (recordedBlob) => {
    this.setState({file: recordedBlob})
  }
  
  clear = () => {
    this.setState({ file: null, error: null });
  };

  render() {
    return (
      <div className="container">
        <h1>Zaloguj się</h1>
        <form onSubmit={this.submitForm}>
          <div className="form-group">
            <label htmlFor="login">Login:</label>
            <input
              className="form-control"
              type="text"
              name="login"
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <ReactMic
              record={this.state.recording}
              className="form-control"
              onData={this.onData}
              onStop={this.onStop}
            />
            <button type="button"
              onClick={(e) => this.setState((prev) => ({...prev, recording: !prev.recording}))}>
              {this.state.recording ? 'Stop' : 'Start'} recording
            </button>
          </div>
          <input type="submit" value="Zaloguj" className="btn btn-primary" />
        </form>
      </div>
    );
  }
}

export default LoginForm;
