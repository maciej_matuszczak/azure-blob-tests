import Link from "next/link";


const Header = () => (
  <nav
    className="navbar navbar-expand-md navbar-dark bg-dark fixed-top"
  >
    <Link href="/">
      <a className="navbar-brand" href="#">
        Navbar
      </a>
    </Link>
    <button
      className="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarsExampleDefault"
      aria-controls="navbarsExampleDefault"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span className="navbar-toggler-icon"></span>
    </button>

    <div className="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul className="navbar-nav mr-auto">
        <li className="nav-item active">
          <Link href="/">
            <a className="nav-link">
              Stron główna <span className="sr-only">(current)</span>
            </a>
          </Link>
        </li>
        <li className="nav-item">
          <Link href="/about">
            <a className="nav-link">About</a>
          </Link>
        </li>
        <li className="nav-item">
          <Link href="/login">
            <a className="nav-link">Logowanie</a>
          </Link>
        </li>
        <li className="nav-item">
          <Link href="/send">
            <a className="nav-link">Wyślij plik</a>
          </Link>
        </li>
      </ul>
    </div>
  </nav>
);

export default Header;
