import { Component } from "react";
import Router from "next/router";
import * as blob from "../azure-storage/azure-storage.blob.min";
import { SAS_TOKEN, SAS_URL } from "../consts";

class FileSender extends Component {
  state = {
    file: null,
    error: null
  };

  sendFile = event => {
    event.preventDefault();
    const service = blob.createBlobServiceWithSas(SAS_URL, SAS_TOKEN);
    service.createContainerIfNotExists(
      "default",
      {
        publicAccessLevel: "blob"
      },
      (containerError, containerResult, containerResponse) => {
        if (!containerError) {
          if (!this.state.file) {
            this.setState({ error: "Wybierz plik" });
            return;
          }
          const { name } = this.state.file;
          service.createBlockBlobFromBrowserFile(
            "default",
            name,
            this.state.file,
            (error, result, response) => {
              if (!error) {
                console.log("SUCCESS", result);
                this.clear();
                Router.push("/");
              } else {
                this.setState({ error });
              }
            }
          );
        } else {
          this.setState({ error: containerError });
        }
      }
    );
  };

  handleFile = ({ target }) => {
    this.setState({ [target.name]: target.files[0] });
  };

  clear = () => {
    this.setState({ file: null, error: null });
  };

  render() {
    return (
      <div className="container">
        <h1>Wyślij plik</h1>
        <form onSubmit={this.sendFile}>
          <div className="form-group">
            <label htmlFor="file">Plik:</label>
            <input
              className="form-control"
              type="file"
              name="file"
              onChange={this.handleFile}
            />
          </div>
          <input type="submit" value="Wyślij" className="btn btn-primary" />
          {this.state.error ? (
            <p className="error">{this.state.error}</p>
          ) : null}
        </form>
      </div>
    );
  }
}

export default FileSender;
